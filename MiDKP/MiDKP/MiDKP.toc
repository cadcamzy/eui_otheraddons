﻿## Interface: 50200
## Title: [BF] MiDKP
## Title-zhCN: [大脚]团队记录
## Title-zhTW: [大腳]團隊記錄
## Notes: |cffbc8f8fA powerful DKP addons developed by 178.|r
## Notes-zhCN: |cff00ff00MiDKP|r                                             |cffbc8f8f艾泽拉斯国家地理自主开发的DKP系统。|r
## Notes-zhTW: |cff00ff00MiDKP|r                                             |cffbc8f8f艾澤拉斯國家地理自主開發的DKP系統。|r
## Version: 5.2.0
## X-BuildNumber: 1049
## Author: Terry,Sharak
## SavedVariables: MiDKP3_Config, MiDKP3B_Options
## X-Revision: BigFoot
## OptionalDeps: BigFoot

locale.lua
localize\tw.lua
localize\cn.lua

util.lua
constants.lua
core.lua
do.lua
widget.lua
ui.lua
event.lua
sys.lua
MiDKPData.lua
MiDKP.xml
