------------------------------------------------------------------------------
-- GridStatusTankCooldown by Slaren
------------------------------------------------------------------------------
GridStatusTankCooldown = Grid:GetModule("GridStatus"):NewModule("GridStatusTankCooldown")
GridStatusTankCooldown.menuName = "Tanking cooldowns"

local tankingbuffs = {
	["DEATHKNIGHT"] = {
		48707, -- ��ħ������
		50461, -- Anti-Magic Zone
		42650, -- Army of the Dead
		77535, -- Blood Shield		
		49222, -- �׹�֮��
		49028, -- Dancing Rune Weapon
		48792, -- ����֮��
		55233, -- ��Ѫ��֮Ѫ
	},
	["DRUID"] = {
		22812,  -- ��Ƥ��
		124769, -- Glyph of Frenzied Regeneration
		102342, -- ��ľ��Ƥ
		106922, -- ������֮��
		132402, -- Savage Defense
		61336,  -- ���汾��
	},
	["HUNTER"] = {
		19263,  -- Deterrence
		63087,  -- Glyph of Raptor Strike
	},
	["MAGE"] = {
		45438,  -- Ice Block
		11426,  -- Ice Barrier
		1463,   -- Incanter's Ward
		115610, -- Temporal Shield
	},
	["MONK"] = {
		115213, -- Avert Harm
		122278, -- ������
		122783, -- ɢħ��
		115308, -- Elusive Brew
		115203, -- ׳����
		115295, -- Guard
		116849, -- ���븿��
		115176, -- ����ڤ��
	},
	["PALADIN"] = {
		31850,  -- ���ȷ�����
		31821,  -- �Ϲ⻷
		498,    -- ʥ����
		642,    -- ʥ����
		86659,  -- Զ����������
		1022,   -- ����֮��
		114039, -- ����֮��
		6940,   -- ����֮��
		20925,  -- Holy Shield
		132403, -- Shield of the Righteous
	},
	["PRIEST"] = {
		47585,  -- Dispersion
		47788,  -- �ػ�֮��
		109964, -- ��껤��
		64901,  -- ϣ��ʥ��
		33206,  -- ʹ��ѹ��
		81782,  -- ����������
	},
	["ROGUE"] = {
		31224,  -- Cloak of Shadows
		5277,   -- Evasion
		1966,   -- Feint
		76577,  -- Smoke Bomb
	},
	["SHAMAN"] = {
		108271, -- Astral Shift
		30823,  -- Shamanistic Rage
		98008,  -- �������ͼ��
		16191,  -- ����֮��ͼ��
		120676, -- �籩֮��ͼ��
		114893, -- Stone Bulwark Totem
	},
	["WARLOCK"] = {
		110913, -- Dark Bargain        
		108359, -- Dark Regeneration
		91711,  -- Nether Ward
		108416, -- Sacrificial Pact
		6229,   -- Shadow Ward
		104773, -- Unending Resolve
	},
	["WARRIOR"] = {
		118038, -- ��������
		12975,  -- �Ƹ�����
		97462,  -- �����ź�
		46947,  -- Safeguard
		112048, -- Shield Barrier
		2565,   -- ���Ƹ�
		871,    -- ��ǽ
		114030, -- Vigilance
	}
}

GridStatusTankCooldown.tankingbuffs = tankingbuffs

-- locals
local GridRoster = Grid:GetModule("GridRoster")
local GetSpellInfo = GetSpellInfo
local UnitBuff = UnitBuff
local UnitDebuff = UnitDebuff
local UnitGUID = UnitGUID

local settings
local spellnames = {}

GridStatusTankCooldown.defaultDB = {
	debug = false,
	alert_tankcd = {
		enable = true,
		color = { r = 1, g = 1, b = 0, a = 1 },
		priority = 99,
		range = false,
		showtextas = "spell",
		active_spellids =  { -- default spells
			47788,  -- �ػ�֮��
			33206,  -- ʹ��ѹ��
			31850,  -- ���ȷ�����
			86659,  -- Զ����������
			61336,  -- ���汾��
			871,    -- ��ǽ
			48792,  -- ����֮��
			115203, -- ׳����
			98008,  -- �������ͼ��
			31821,  -- �Ϲ⻷
			81782,  -- ����������
			97462,  -- �����ź�
			116849, -- ���븿��
			6940,   -- ����֮��
			102342, -- ��ľ��Ƥ
			2812,   -- ��Ƥ��
			106922, -- ������֮��
			49222,  -- �׹�֮��
			48707,  -- ��ħ������
			55233,  -- ��Ѫ��֮Ѫ
			122278, -- ������
			122783, -- ɢħ��
			115176, -- ����ڤ��
			498,    -- ʥ����
			114039, -- ����֮��
			1022,   -- ����֮��
			642,    -- ʥ����
			118038, -- ��������
			2565,   -- ���Ƹ�
			12975,  -- �Ƹ�����
			109964, -- ��껤��
			64901,  -- ϣ��ʥ��
			16191,  -- ����֮��ͼ��
		},
		inactive_spellids = { -- used to remember priority of disabled spells
		}
	}
}

local myoptions = {
	["gstcd_header_1"] = {
		type = "header",
		order = 200,
		name = "Options",
	},
	["showtextas"] = {
		order = 201,
		type = "select",
		name = "Show text as",
		desc = "Text to show when assigned to an indicator capable of displaying text",
		values = { ["caster"] = "Caster name", ["spell"] = "Spell name" },
		style = "radio",
		get = function() return GridStatusTankCooldown.db.profile.alert_tankcd.showtextas end,
		set = function(_, v) GridStatusTankCooldown.db.profile.alert_tankcd.showtextas = v end,
	},
	["gstcd_header_2"] = {
		type = "header",
		order = 203,
		name = "Spells",
	},
	["spells_description"] = {
		type = "description",
		order = 204,
		name = "Check the spells that you want GridStatusTankCooldown to keep track of. Their position on the list defines their priority in the case that a unit has more than one of them.",
	},
	["spells"] = {
		type = "input",
		order = 205,
		name = "Spells",
		control = "GSTCD-SpellsConfig",
	},
}

function GridStatusTankCooldown:OnInitialize()
	self.super.OnInitialize(self)
	
	for class, buffs in pairs(tankingbuffs) do
		for _, spellid in pairs(buffs) do
			local sname = GetSpellInfo(spellid)
			spellnames[spellid] = sname
		end
	end
	
	self:RegisterStatus("alert_tankcd", "Tanking cooldowns", myoptions, true)

	settings = self.db.profile.alert_tankcd

	-- delete old format settings
	if settings.spellids then
		settings.spellids = nil
	end
	
	-- remove old spellids
	for p, aspellid in ipairs(settings.active_spellids) do
		local found = false
		for class, buffs in pairs(tankingbuffs) do
			for _, spellid in pairs(buffs) do
				if spellid == aspellid then
					found = true
					break
				end				
			end
		end
		
		if not found then
			table.remove(settings.active_spellids, p)
		end
		
		-- remove duplicates
		for i = #settings.active_spellids, p + 1, -1 do
			if settings.active_spellids[i] == aspellid then
				table.remove(settings.active_spellids, i)
			end
		end
	end
end

function GridStatusTankCooldown:OnStatusEnable(status)
	if status == "alert_tankcd" then
		self:RegisterEvent("UNIT_AURA", "ScanUnit")
		self:RegisterEvent("Grid_UnitJoined")
		-- self:ScheduleRepeatingEvent("GridStatusTankCooldown:UpdateAllUnits", self.UpdateAllUnits, 0.5, self)
		self:UpdateAllUnits()
	end
end

function GridStatusTankCooldown:OnStatusDisable(status)
	if status == "alert_tankcd" then
		self:UnregisterEvent("UNIT_AURA")
		self:UnregisterEvent("Grid_UnitJoined")

		--self:CancelScheduledEvent("GridStatusTankCooldown:UpdateAllUnits")
		self.core:SendStatusLostAllUnits("alert_tankcd")
	end
end

function GridStatusTankCooldown:Grid_UnitJoined(guid, unitid)
	self:ScanUnit("Grid_UnitJoined", unitid, guid)
end

function GridStatusTankCooldown:UpdateAllUnits()
	for guid, unitid in GridRoster:IterateRoster() do
		self:ScanUnit("UpdateAllUnits", unitid, guid)
	end
end

function GridStatusTankCooldown:ScanUnit(event, unitid, unitguid)
	unitguid = unitguid or UnitGUID(unitid)
	if not GridRoster:IsGUIDInRaid(unitguid) then
		return
	end

	for _, spellid in ipairs(settings.active_spellids) do
		local name, _, icon, count, _, duration, expirationTime, caster = UnitBuff(unitid, spellnames[spellid])
		
		-- Used to check for debuffs when Argent Defender was a debuff - it is not necessary anymore
		--[[
		if not name then
			name, _, icon, count, _, duration, expirationTime, caster = UnitDebuff(unitid, spellnames[spellid])
		end
		]]

		if name then
			local text 
			if settings.showtextas == "caster" then
				if caster then
					text = UnitName(caster)
				end
			else
				text = name
			end

			self.core:SendStatusGained(unitguid, 
						"alert_tankcd",
						settings.priority,
						(settings.range and 40),
						settings.color,
						text,
						0,							-- value
						nil,						-- maxValue
						icon,						-- icon
						expirationTime - duration,	-- start
						duration,					-- duration
						count)						-- stack
			return
		end
	end

	self.core:SendStatusLost(unitguid, "alert_tankcd")
end
