## Interface: 50200
## Title: GridStatusThreat
## Author: kunda
## Notes: Adds a simple percentage threat status to Grid.
## Version: 50001-1
## Dependencies: Grid
## X-Curse-Packaged-Version: 50001-1
## X-Curse-Project-Name: GridStatusThreat
## X-Curse-Project-ID: grid-status-threat
## X-Curse-Repository-ID: wow/grid-status-threat/mainline

GridStatusThreat-localization-enUS.lua
GridStatusThreat-localization-zhCN.lua
GridStatusThreat-localization-zhTW.lua

GridStatusThreat.lua