local _, GBS = ...

local media = LibStub("LibSharedMedia-3.0", true)

local MBL = GridManaBarsLocale
local L = GBS.L 

local GridFrame = Grid:GetModule("GridFrame")

GridBorderStyle = Grid:NewModule("GridBorderStyle")

GridBorderStyle.defaultDB = {                --以下修改默认值
	style = "OverlapStyle",
	shadow = true,
	debug = false,
	classColor = false,
	texSet = true,
	tex = {
		["Elv"] = "ElvBar",
		["OverlapStyle"] = "TukNorm",
	},
}

local Styles, Textures
local style

---------------------
-- some local libs --
---------------------

local function getTexture(texType, texName)
	if not media:IsValid(texType, texName) then 	
		local tex = Textures[texName]
		if not tex then error(("%s: file not found."):format(texName)) end
		media:Register(texType, texName, tex)
	end
	return media:Fetch(texType, texName)
end

local function dropShadow(f, outset, parent)
	GridBorderStyle:Debug("Drop shadow for ", f:GetName() )
		
	if not parent then parent = f end
	outset = outset or 0
	local shadow = CreateFrame("Frame", nil, parent)
	shadow:SetFrameStrata("BACKGROUND")
	shadow:SetFrameLevel(1)
	shadow:SetPoint("TOPLEFT", f, "TOPLEFT", -3 - outset, 3 + outset)
	shadow:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", 3 + outset, -3 - outset)
	shadow:SetBackdrop( {
		edgeFile = getTexture("background", "TukGlow"), edgeSize = 3,
		insets = { left = 5, right = 5, top = 5, bottom = 5},
	})
	shadow:SetBackdropColor(0, 0, 0, 0)
	shadow:SetBackdropBorderColor(0, 0, 0, .8)
	
	if GridBorderStyle.db.profile.shadow then 		
		shadow:Show() 
	else 
		shadow:Hide()
	end		
	return shadow
end

local function addBorderFrame(area, bgtab, outset, parent)
	if not parent then parent = area end
	local f = CreateFrame("Frame", nil, parent)
	f:SetFrameStrata("LOW")
	f:SetFrameLevel(2)
	f:SetBackdrop(bgtab)
	f:SetBackdropColor(unpack(Styles[style].BackdropColor))
	f:SetBackdropBorderColor(unpack(Styles[style].BorderColor))
	f:SetPoint("TOPLEFT", area, "TOPLEFT", -outset, outset)
	f:SetPoint("BOTTOMRIGHT", area, "BOTTOMRIGHT", outset, -outset)

	return f
end

------------------------
--	   Styles DB      --
------------------------

Textures = {
	["TukGlow"] = [[Interface\Addons\GridBorderStyle\medias\glowTex]],
	["TukBlank"] = [[Interface\Addons\GridBorderStyle\medias\blank]],
	["ElvBar"] = [[Interface\Addons\GridBorderStyle\medias\normTex]],
	["TukNorm"] = [[Interface\Addons\GridBorderStyle\medias\tukNorm]],
	["QulightTex"] = [[Interface\Addons\GridBorderStyle\medias\QulightTex]],
}


Styles = {
		["None"] = {
			min = 10,
			ShadowOutset = -2,
		},
		["Elv"] = {
			type = "Divide",
			min = 15,
			StatusbarTex = "ElvBar",
			Backdrop = { bgFile = getTexture("background","TukBlank"),
						 tile = false,
						 tileSize = 0 ,
						 edgeFile = getTexture("background","TukBlank"), 
						 insets = {left = -1, right = -1, top = -1, bottom = -1 } },
			BackdropColor = {.1, .1, .1},
			BorderColor = {.3, .3, .3},
			GapbarTex = "TukBlank",
		},
		["Qulight"] = {
			type = "Isolate",
			min = 5,
			MBAnchorP = "CENTER",
			offset = 6,
			ShadowOutset = -3,
			StatusbarTex = "QulightTex",
			Backdrop = { bgFile = getTexture("background","TukBlank"),
						 tile = false,
						 tileSize = 0 ,
						 edgeFile = getTexture("background","TukBlank"), 
						 edgeSize = 1,
						 insets = {left = -1, right = -1, top = -1, bottom = -1 } },
			BackdropColor = {.02, .02, .02},
			BorderColor = {.3, .3, .3},
		},
		["OverlapStyle"] = {
			type = "Overlap",
			min = 5,
			MBLengthPer = .75,
			ShadowOutset = -3,
			StatusbarTex = "ElvBar",
			Backdrop = { bgFile = getTexture("background","TukBlank"),
						 tile = false,
						 tileSize = 0 ,
						 edgeFile = getTexture("background","TukBlank"), 
						 edgeSize = 1,
						 insets = {left = -1, right = -1, top = -1, bottom = -1 } },
			BackdropColor = {.02, .02, .02},
			BorderColor = {.3, .3, .3},
		},
		["LayersStyle"] = {
			type = "Layers",
			min = 5,
			offset = 7,
			ShadowOutset = -3,
			StatusbarTex = "ElvBar",
			Backdrop = { bgFile = getTexture("background","TukBlank"),
						 tile = false,
						 tileSize = 0 ,
						 edgeFile = getTexture("background","TukBlank"), 
						 edgeSize = 1,
						 insets = {left = -1, right = -1, top = -1, bottom = -1 } },
			BackdropColor = {.02, .02, .02},
			BorderColor = {.3, .3, .3},
		}
}

--------------------------------
-- register into Grid options --
--------------------------------

local styleOptions = {
	type = "group",
	name = L["Style"],
	desc = "Style Option",
	args = {
		["Chosen Style"] = {
			type = "select",
			order = 5,
			name = L["Choose a Style"],
			desc = "Choose a style",
			get = function() 
					return GridBorderStyle.db.profile.style
				  end,
			set = function(_,v) 
					GridBorderStyle.db.profile.style = v 
				  end,
			values= {["Elv"] = "ElvUI",["Qulight"] = "Qulight" , ["OverlapStyle"] = "OverlapStyle", ["None"] = "None",
					["LayersStyle"] = "LayersStyle" },
		},
		["Reloadui"] = {
			type = "execute",
			order = 6,
			name = L["Reload"],
			width = "single",
			desc = L["Reload UI to Apply Style"],
			func = function() ReloadUI() end,
		},
		["Shadow"] = {
			type = "toggle",
			order = 0,
			name = L["Frame Shadow"],
			desc = L["Turn on/off border shade"],
			get = function() return GridBorderStyle.db.profile.shadow end,
			set = function(_,v) 
					GridBorderStyle.db.profile.shadow = v
					if v then 
						GridFrame:WithAllFrames(function(f) 
													f.Shadow:Show() 
													if f.MBShadow then f.MBShadow:Show() end 
												end)
					else 
						GridFrame:WithAllFrames(function(f) 
													f.Shadow:Hide() 
													if f.MBShadow then f.MBShadow:Hide() end 
												end)
					end
				  end,
		},
		["ClassColor"] = {
			type = "toggle",
			order = 1,
			name = L["Use class color"],
			desc = L["Color manabar by class"],
			get = function() return GridBorderStyle.db.profile.classColor end,
			set = function() 
				GridBorderStyle.db.profile.classColor = not GridBorderStyle.db.profile.classColor
				GridMBStatus:UpdateAllUnits()
				  end,
		},
	}
}
Grid.options.args["Styles"] = styleOptions

--------------------------------
--   	 init and shares      --
--------------------------------

function GridBorderStyle:PostInitialize()
	style = self.db.profile.style
	if not style or not Styles[style] then return end
	
	for k, v in pairs(Textures) do
		if not media:IsValid("statusbar", k) then
			media:Register("statusbar", k, v)
		end
	end
	if Styles[style].StatusbarTex then
		GridFrame.db.profile.texture = GridBorderStyle.db.profile.tex[style] or 
										Styles[style].StatusbarTex
		GridBorderStyle.db.profile.tex[style] = GridFrame.db.profile.texture 
--		GridFrame:WithAllFrames(function(f) 
--				f:SetFrameTexture(media:Fetch("statusbar", GridFrame.db.profile.texture)) end)
	end
	GridFrame.options.args.bar.args.texture.set = function(_, v)
			GridFrame.db.profile.texture = v
			local texture = media:Fetch("statusbar", v)
			for k, v in pairs(GridBorderStyle.db.profile.tex) do
			end
			GridBorderStyle.db.profile.tex[style] = v
			GridFrame:WithAllFrames(function(f) f:SetFrameTexture(texture) 
				f.ManaBar:SetStatusBarTexture(texture)
				if Styles[style]=="Layers" then
					f.BarMBG:SetTexture(0,0,0,0)
				end
			   end)
		end

	Grid.options.args["manabar"].args["Manabar size"].min = Styles[style].min
	GridMBFrame.db.profile.size = 
						GridMBFrame.db.profile.size < Styles[style].min/100 and Styles[style].min/100 
						or GridMBFrame.db.profile.size
	hooksecurefunc(GridFrame, "InitializeFrame", GridBorderStyle.ShadowHook)
	if GridMBFrame then 
		GridMBFrame.SetManaBarColor = GridBorderStyle.SetManaBarColor
	end
	
	GridMBStatus.UpdateUnitPower = GridBorderStyle.UpdateUnitPower
		
	if style == "None" then return end
	
	
	hooksecurefunc(GridFrame.prototype, "PlaceIndicators", GridBorderStyle.RelocateIndicators)
	hooksecurefunc(GridFrame.prototype, "SetBorderSize", GridBorderStyle.FrameHook)
	hooksecurefunc(GridFrame.prototype, "ClearIndicator", GridBorderStyle.BorderColorHook)

	-- in "Divide" type style, just pick the original backdrop to spawn border.
	if Styles[style].type == "Divide" then
	
		if not (GridMBFrame and Styles[style].GapbarTex) then return end
		GridMBFrame.ReAnchor = GridBorderStyle.ReAnchorOverride_Div
		GridMBFrame.HideManaBar = GridBorderStyle.HideManabarOverride_Div

-- in "Isolate" type style, create a border frame under GridFrame & GridMBFrame each.
-- "Overlap" type is much like "Isolate" ,except that Power Bar frame level is higher. 
-- so I intergrate them together in funcs implementation level.
	elseif Styles[style].type == "Isolate" or Styles[style].type == "Overlap" or Styles[style].type == "Layers" then
		if not GridMBFrame then return end
	--	hooksecurefunc(GridMBFrame, "CreateFrames", GridBorderStyle.MBFrameHook)   
	--  any way for chain hooking ? 
		GridMBFrame.ReAnchor = GridBorderStyle.ReAnchorOverride_Iso
		GridMBFrame.HideManaBar = GridBorderStyle.HideManabarOverride_Iso
		if Styles[style].Backdrop then 
			hooksecurefunc(GridFrame.prototype, "SetIndicator", GridBorderStyle.SetIsoBorderInidicator)
		end
		
		if Styles[style].type == "Overlap" or Styles[style].type == "Isolate" then 
			GridMBFrame.SetManaBarHeight = GridBorderStyle.SetManaBarHeight
			GridMBFrame.SetManaBarWidth = GridBorderStyle.SetManaBarWidth
		end
		
		if Styles[style].type == "Layers"  then
			Grid.options.args["manabar"].args["Manabar side"].values=
				{["Left"] = L["Topleft"], ["Top"] = L["Topright"], ["Right"] = L["Bottomright"], ["Bottom"] = L["Bottomleft"] }	
		else
			Grid.options.args["manabar"].args["Manabar side"].values=
				{["Left"] = MBL["Left"], ["Top"] = MBL["Top"], ["Right"] = MBL["Right"], ["Bottom"] = MBL["Bottom"] }
		end
	end
	self:Debug("GridBorderStyle Loaded")
end

function GridBorderStyle.RelocateIndicators(f)
	f.IconBG:SetPoint("CENTER", f.BarBG, "CENTER")
	if f.textorientation == "HORIZONTAL" then
		f.Text:ClearAllPoints()
		f.Text:SetPoint("LEFT", f.BarBG, "LEFT", 2, 0)
		if f.enableText2 then
			f.Text2:ClearAllPoints()
			f.Text2:SetPoint("RIGHT", f.BarBG, "RIGHT", -2, 0)
		end
	else
		f.Text:ClearAllPoints()
		if f.enableText2 then
			f.Text:SetPoint("BOTTOM", f.BarBG, "CENTER")
			f.Text2:ClearAllPoints()
			f.Text2:SetPoint("TOP", f.BarBG, "CENTER")
		else
			f.Text:SetPoint("CENTER", f.BarBG, "CENTER")
		end
	end
end

function GridBorderStyle.SetManaBarColor(frame, col)
		frame.ManaBar:SetStatusBarColor(col.r,col.g,col.b,col.a)
		if Styles[style].type == "Layers" or Styles[style].type == "Overlap" then
			frame.NewMBG:SetVertexColor(col.r * .2, col.g * .2, col.b * .2,col.a)
		else
			frame.BarMBG:SetVertexColor(col.r * .2, col.g * .2, col.b * .2,col.a)
		end
end

function GridBorderStyle.FrameHook(frame, ...)
	local f = frame
	if Styles[style].type == "Divide" then 
		if  Styles[style].Backdrop then
			local fbg = f:GetBackdrop()
			for k,v in pairs(Styles[style].Backdrop) do
				fbg[k] = v
			end
			local r, g, b, a = f:GetBackdropBorderColor()
			f:SetBackdrop(fbg)
			f:SetBackdropBorderColor(r, g, b, a)
		end
		if Styles[style].BackdropColor then
			f:SetBackdropColor(unpack(Styles[style].BackdropColor))
		end
	else
		f.HBarBorder = addBorderFrame(f.BarBG, Styles[style].Backdrop, 2, f)
		GridBorderStyle:Debug("border setup")
		f:SetBackdropColor(0,0,0,0)
		f:SetBackdropBorderColor(0, 0, 0, 0)
	end

end

function GridBorderStyle.ShadowHook(obj, f)
	if Styles[style].type == "Divide" or style == "None" then 
		f.Shadow = dropShadow(f, Styles[style].ShadowOutset)
	else
		if Styles[style].Backdrop then
			f.Shadow = 
				dropShadow(f.BarBG, Styles[style].ShadowOutset + GridFrame.db.profile.borderSize + 4, f)
		else
			f.Shadow = 
				dropShadow(f.BarBG, Styles[style].ShadowOutset + GridFrame.db.profile.borderSize + 1, f)
		end
	end
end

function GridBorderStyle.BorderColorHook(f, indicator)
	if indicator == "border" and Styles[style].BorderColor then
		if Styles[style].type == "Divide" then
			f:SetBackdropBorderColor(unpack(Styles[style].BorderColor))
		else 
			if not f.HBarBorder or not f.MBBorder then return end
			f.HBarBorder:SetBackdropBorderColor(unpack(Styles[style].BorderColor))
			f.MBBorder:SetBackdropBorderColor(unpack(Styles[style].BorderColor))
			GridBorderStyle:Debug("border indicator cleared")
		end
	end
end

function GridBorderStyle.UpdateUnitPower(object, unitid)
	local cur, max = UnitPower(unitid), UnitPowerMax(unitid)
	local priority = GridMBStatus.db.profile.unit_mana.priority

	if cur==max then
		priority=1
	end

	local powerType = UnitPowerType(unitid)
	local col = nil

    local unitGUID = UnitGUID(unitid)
	
	if GridBorderStyle.db.profile.classColor == true then
		col = GridMBStatus.core:UnitColor(unitGUID)
	else
		if powerType == 3 or powerType == 2 then
			col = GridMBStatus.db.profile.ecolor
		elseif powerType == 6 then
			col = GridMBStatus.db.profile.dcolor
		elseif powerType == 1 then
			col = GridMBStatus.db.profile.rcolor
		else
			col = GridMBStatus.db.profile.color
		end
	end

	GridMBStatus.core:SendStatusGained(
        unitGUID, "unit_mana",
        priority,
        nil,
		col,
        nil,
        cur,max,
        nil
    )	
end

---------------------------------------------
--    Divide-Type funcs Implementation     --
---------------------------------------------

function GridBorderStyle.ReAnchorOverride_Div(frame, side)
		local f = frame
		if not f.ManaBar then return end

		if not f.Gap then
			f.Gap = f:CreateTexture(nil, "BORDER")
			f.Gap:SetTexture( getTexture("background", Styles[style].GapbarTex))
			f.Gap:SetVertexColor(unpack(Styles[style].BorderColor))
		end

		f.BarBG:ClearAllPoints()
		f.BarMBG:ClearAllPoints()
		
		local b = GridFrame.db.profile.borderSize+1

		if side == "Right" then
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.Gap:SetPoint("TOPLEFT",f.BarBG,"TOPRIGHT", 1, 0)
			f.Gap:SetPoint("BOTTOMRIGHT",f.BarBG,"BOTTOMRIGHT", 2, 0)
			f.BarMBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.BarMBG:SetPoint("BOTTOMLEFT", f.Gap, "BOTTOMRIGHT", 1, 0)
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Left" then
			f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.Gap:SetPoint("TOPLEFT",f.BarBG,"TOPLEFT", -2, 0)
			f.Gap:SetPoint("BOTTOMRIGHT",f.BarBG,"BOTTOMLEFT", -1, 0)
			f.BarMBG:SetPoint("BOTTOMRIGHT", f.Gap, "BOTTOMLEFT", -1, 0)
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Bottom" then
			f.Gap:SetPoint("TOPLEFT",f.BarBG,"BOTTOMLEFT", 0, -1)
			f.Gap:SetPoint("BOTTOMRIGHT",f.BarBG,"BOTTOMRIGHT", 0, -2)
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarMBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.BarMBG:SetPoint("TOPRIGHT", f.Gap, "BOTTOMRIGHT", 0, -1)
			f.ManaBar:SetOrientation("HORIZONTAL")
		else
			f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.Gap:SetPoint("TOPLEFT",f.BarBG,"TOPLEFT", 0, 2)
			f.Gap:SetPoint("BOTTOMRIGHT",f.BarBG,"TOPRIGHT", 0, 1)
			f.BarMBG:SetPoint("BOTTOMRIGHT", f.Gap, "TOPRIGHT", 0, 1)
			f.ManaBar:SetOrientation("HORIZONTAL")
		end
			
			frame:SetFrameWidth(frame:GetWidth() )    
			frame:SetFrameHeight(frame:GetHeight() )  
end

function GridBorderStyle.HideManabarOverride_Div(frame, v)
			local f = frame			
			if not f.ManaBar then return end    
			f.hideMB = v
			if v then
				local side = GridMBFrame.db.profile.side
				if side == "Left" or side == "Right" then
					local width = frame:GetWidth() 
					f.BarBG:SetWidth(width - (GridFrame.db.profile.borderSize+1)*2) 
				else
					local height = frame:GetHeight() 
					f.BarBG:SetHeight(height - (GridFrame.db.profile.borderSize+1)*2) 
				end
				f.Gap:Hide()
				f.ManaBar:Hide()
				f.BarMBG:Hide()
			else
				f.Gap:Show()
				f.ManaBar:Show()
				f.BarMBG:Show()
				GridMBFrame.SetManaBarHeight(frame, frame:GetHeight()  - (GridFrame.db.profile.borderSize+1)*2)
				GridMBFrame.SetManaBarWidth(frame, frame:GetWidth()  - (GridFrame.db.profile.borderSize+1)*2)
			end		
end	

----------------------------------
--   Isolate,OverLap,Layers     --
----------------------------------

function GridBorderStyle.SetIsoBorderInidicator(f, indicator, color, text, value, maxValue, texture, start, duration, stack)
	if not color then
		color = COLOR_WHITE
	end
	if not f.ManaBar then return end
	if indicator == "border" then 
		f:SetBackdropBorderColor(0, 0, 0, 0)
		f.HBarBorder:SetBackdropBorderColor(color.r, color.g, color.b, color.a or 1)
		f.MBBorder:SetBackdropBorderColor(color.r, color.g, color.b, color.a or 1)
	end
end

function GridBorderStyle.HideManabarOverride_Iso(frame, v)
    local f = frame
    if not f.ManaBar then return end    

    f.hideMB = v

    if v then
        local side = GridMBFrame.db.profile.side
        if side == "Left" or side == "Right" then
            local width = frame:GetWidth() 
            f.BarBG:SetWidth(width - (GridFrame.db.profile.borderSize+1)*2) --resize health bar to whole frame size
        else
            local height = frame:GetHeight() 
            f.BarBG:SetHeight(height - (GridFrame.db.profile.borderSize+1)*2) --resize health bar to whole frame size
        end
        f.ManaBar:Hide()
        f.BarMBG:Hide()
		if f.MBBorder then f.MBBorder:Hide() end
		f.MBShadow:Hide()
    else
        f.ManaBar:Show()
        f.BarMBG:Show()
		if f.MBBorder then f.MBBorder:Show() end
		if f.MBShadow and GridBorderStyle.db.profile.shadow then f.MBShadow:Show() end
        GridMBFrame.SetManaBarHeight(frame, frame:GetHeight()  - (GridFrame.db.profile.borderSize+1)*2)
        GridMBFrame.SetManaBarWidth(frame, frame:GetWidth()  - (GridFrame.db.profile.borderSize+1)*2)
    end
end

function GridBorderStyle.ReAnchorOverride_Iso(frame, side)
	local f = frame
	if not f.ManaBar then return end
	
	if Styles[style].Backdrop then 
		f.MBBorder = addBorderFrame(f.BarMBG, Styles[style].Backdrop, 2, f)
	--	print("BarMBG Border spawned!!!")
	--	f.MBBorder:Hide()
		f.MBShadow =f.MBShadow or 
					dropShadow(f.BarMBG,Styles[style].ShadowOutset + GridFrame.db.profile.borderSize + 4, f)
	else 
		f.MBShadow =f.MBShadow or
					dropShadow(f.BarMBG,Styles[style].ShadowOutset + GridFrame.db.profile.borderSize + 1, f)
	end	

	f.BarBG:ClearAllPoints()
	f.BarMBG:ClearAllPoints()
	
	local b = GridFrame.db.profile.borderSize+1
    
	if Styles[style].type == "Isolate" then 
		local offset = Styles[style].offset
		if f.MBBorder then 
			local offset = (offset and offset < 6 or not offset) and 5 or offset or 0
		end

		if side == "Right" then
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.BarMBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.BarBG:SetPoint("BOTTOMRIGHT", f.BarMBG, "BOTTOMLEFT", -offset, 0 )
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Left" then
			f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.BarBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", -b,b)
			f.BarBG:SetPoint("BOTTOMLEFT", f.BarMBG, "BOTTOMRIGHT", offset, 0)
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Bottom" then
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
--			f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.BarMBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
--			f.BarMBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT",-b,b)
			f.BarBG:SetPoint("BOTTOMRIGHT", f.BarMBG, "TOPRIGHT", 0, offset)
			f.ManaBar:SetOrientation("HORIZONTAL")
		else
			f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.BarBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT",-b,b)
			f.BarBG:SetPoint("TOPRIGHT", f.BarMBG, "BOTTOMRIGHT", 0, -offset)
			f.ManaBar:SetOrientation("HORIZONTAL")
		end
		frame:SetFrameWidth(frame:GetWidth() )    
		frame:SetFrameHeight(frame:GetHeight() )  

	elseif Styles[style].type == "Overlap" then

		if f.MBBorder then 			
			f.MBBorder:SetFrameStrata("MEDIUM")
			f.MBBorder:SetFrameLevel(f.Bar:GetFrameLevel()+1)
		end
		f.NewMBG = f.MBBorder:CreateTexture(nil, "BORDER")
		f.NewMBG:SetAllPoints(f.BarMBG)
		f.NewMBG:SetTexture([[Interface\Addons\GridBorderStyle\medias\normTex]])
		f.ManaBar:SetFrameLevel(f.Bar:GetFrameLevel()+2)


		
	--	print("Overlap type reanchor")
		if side == "Right" then
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.BarMBG:SetPoint("CENTER", f.BarBG, "RIGHT")
			f.BarMBG:SetPoint("RIGHT", f, "RIGHT")
			f.BarMBG:SetHeight(Styles[style].MBLengthPer * f:GetHeight())
			
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Left" then
			f.BarBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", -b, b)
			f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
			f.BarMBG:SetPoint("CENTER", f.BarBG, "LEFT")
			f.BarMBG:SetPoint("LEFT", f, "LEFT")
			f.BarMBG:SetHeight(Styles[style].MBLengthPer * f:GetHeight())
			f.ManaBar:SetOrientation("VERTICAL")
		elseif side == "Bottom" then
			f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
			f.BarMBG:SetPoint("CENTER", f.BarBG, "BOTTOM")
			f.BarMBG:SetPoint("BOTTOM", f, "BOTTOM")
			f.BarMBG:SetWidth(Styles[style].MBLengthPer * f:GetWidth())
			f.ManaBar:SetOrientation("HORIZONTAL")
		else
			f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
			f.BarMBG:SetPoint("CENTER", f.BarBG, "TOP")
			f.BarMBG:SetPoint("TOP", f, "TOP")
			f.BarMBG:SetWidth(Styles[style].MBLengthPer * f:GetWidth())
			f.ManaBar:SetOrientation("HORIZONTAL")
		end
		frame:SetFrameWidth(frame:GetWidth() )    
		frame:SetFrameHeight(frame:GetHeight() )  
		
	end
	

	
	if Styles[style].type == "Layers" then
		local offset = Styles[style].offset
		
		f.ManaBar:SetFrameLevel(1)
		if f.HBarBorder then 			
			f.HBarBorder:SetFrameStrata("MEDIUM")
			f.HBarBorder:SetFrameLevel(2)
		end
		f.BarMBG:SetTexture(0,0,0,0)
		f.NewMBG = f.MBBorder:CreateTexture(nil, "BORDER")
		f.NewMBG:SetAllPoints(f.BarMBG)
		f.NewMBG:SetTexture([[Interface\Addons\GridBorderStyle\medias\normTex]])
		
		if side == "Right" then
		--bottomright
		f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT",b,-b)
		f.BarBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", -offset, offset)
		f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT", offset, -offset)
		f.BarMBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", -b, b)
		elseif side == "Top" then
		--topright
		f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT",b,b)
		f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT", -offset, -offset)
		f.BarMBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT", offset, offset)
		f.BarMBG:SetPoint("TOPRIGHT", f, "TOPRIGHT", -b, -b)
		elseif side == "Left" then
		--topleft
		f.BarBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT",-b,b)
		f.BarBG:SetPoint("TOPLEFT", f, "TOPLEFT", offset, -offset)
		f.BarMBG:SetPoint("BOTTOMRIGHT", f, "BOTTOMRIGHT", -offset, offset)
		f.BarMBG:SetPoint("TOPLEFT", f, "TOPLEFT", b, -b)
		else
		--bottomleft
		f.BarBG:SetPoint("TOPRIGHT", f, "TOPRIGHT",-b,-b)
		f.BarBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT", offset, offset)
		f.BarMBG:SetPoint("TOPRIGHT", f, "TOPRIGHT", -offset, -offset)
		f.BarMBG:SetPoint("BOTTOMLEFT", f, "BOTTOMLEFT", b, b)		
		end
	end
end

function GridBorderStyle.SetManaBarHeight(frame, height)
	if not frame.ManaBar then return end
    
	if frame.hideMB==true then return end

	local f=frame;
	local side = GridMBFrame.db.profile.side

	if not (side == "Bottom" or side == "Top") then 
		if Styles[style].type == "Isolate" then
			f.BarMBG:SetHeight(height)
		elseif Styles[style].type == "Overlap" then
			f.BarMBG:SetHeight(height * Styles[style].MBLengthPer)
		end
		return
	end

	local MBSize = GridMBFrame.db.profile.size
	if Styles[style].type == "Overlap" then
		f.BarMBG:SetHeight(MBSize * f:GetHeight())
		local newHeight = height * (1 - MBSize/2)
		f.BarBG:SetHeight(newHeight)
		f.Bar:SetHeight(newHeight)
		f.HealingBar:SetHeight(newHeight)
	elseif Styles[style].type == "Isolate" then
		f.BarMBG:SetHeight(MBSize * f:GetHeight())
	end
end

function GridBorderStyle.SetManaBarWidth(frame, width)
	if not frame.ManaBar then return end

	if frame.hideMB==true then return end
	
	local f=frame;
	local side = GridMBFrame.db.profile.side
	
	if not (side == "Right" or side == "Left") then 
		if Styles[style].type == "Isolate" then
			f.BarMBG:SetWidth(width)
		elseif Styles[style].type == "Overlap" then
			f.BarMBG:SetWidth(width * Styles[style].MBLengthPer)
		end
		return
	end
	
	local MBSize = GridMBFrame.db.profile.size
	if Styles[style].type == "Overlap" then
		f.BarMBG:SetWidth(MBSize * f:GetWidth())
        local newWidth = width * (1 - MBSize/2)
        f.BarBG:SetWidth(newWidth)
        f.Bar:SetWidth(newWidth)
        f.HealingBar:SetWidth(newWidth)
	elseif Styles[style].type == "Isolate" then
		f.BarMBG:SetWidth(MBSize * f:GetWidth())
	end
end 



--[[ for extending styles
local function RegisterStyle(styleName, styleTab)
	if not type(styleTab) == "table" then error(("Style should be defined in table, not in %s"):format(type(styleTab)))
	if Styles[styleName] then error(("Style -%s- already exists!"):format(styleName)) end
	Style[styleName] = styleTab
end
ns.RegisterStyle = RegisterStyle

local function RegisterTexture(texName, file)
	if Textures(texName) then return end
	Textures[texName] = file
end
ns.RegisterTexture = RegisterTexture]]

