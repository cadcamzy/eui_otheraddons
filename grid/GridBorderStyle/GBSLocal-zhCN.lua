﻿local _, GBS = ...

if GetLocale() ~= "zhCN" then return end

do
	local L = GBS.L

	L["Style"] = "边框样式"
	L["Choose a Style"] = "选择样式"
	L["Reload"] = "重载界面"
	L["Reload UI to Apply Style"] = "重载以更新边框样式"
	L["Frame Shadow"] = "边框阴影"
	L["Turn on/off border shade"] = "阴影开关"
	L["Use class color"] = "使用职业颜色"
	L["Color manabar by class"] = "用职业颜色渲染发力条"

	L["Topleft"] = "左上"
	L["Topright"] = "右上"
	L["Bottomright"] = "右下"
	L["Bottomleft"] = "左下"
	L["Sign"] = "hokohuang @ www.ngacn.cc \n 船歌 @tw-聖光之願 <冰封十字軍>" 
end