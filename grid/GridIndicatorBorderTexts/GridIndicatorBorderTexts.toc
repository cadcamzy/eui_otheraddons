﻿## Interface: 50200
## Title: GridIndicatorBorderTexts
## Author: sirjoga
## Version: 0.5
## Dependencies: Grid
## X-Curse-Packaged-Version: 0.5
## X-Curse-Project-Name: GridIndicatorBorderTexts
## X-Curse-Project-ID: gridindicatorbordertexts
## X-Curse-Repository-ID: wow/gridindicatorbordertexts/mainline

#@no-lib-strip@
libs\AceLocale-3.0\AceLocale-3.0.xml
#@end-no-lib-strip@

locals\locals.xml
core.lua

