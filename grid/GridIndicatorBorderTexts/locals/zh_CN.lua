﻿local L = LibStub("AceLocale-3.0"):NewLocale("GridIndicatorBorderTexts","zhCN")
if not L then return end

L["FONT"] = "字体"
L["FONT_DESC"] = "设置字体"
L["FONT_SIZE"] = "字体大小"
L["FONT_SIZE_DESC"] = "设置字体大小"
L["GRP_CORNER"] = "角落数字"
L["GRP_SIDE"] = "四边数字"
L["OPTIONS_TITLE"] = "边角数字"
L["OUTLINE"] = "描边"
L["OUTLINE_DESC"] = "使用描边"
L["OUTLINE_NONE"] = "无"
L["OUTLINE_THICK"] = "粗"
L["OUTLINE_THIN"] = "细"
L["TITLE_BOTTOM"] = "下方数字"
L["TITLE_BOTTOMLEFT"] = "左下数字"
L["TITLE_BOTTOMRIGHT"] = "右下数字"
L["TITLE_LEFT"] = "左边数字"
L["TITLE_RIGHT"] = "右边数字"
L["TITLE_TOP"] = "上方数字"
L["TITLE_TOPLEFT"] = "左上数字"
L["TITLE_TOPRIGHT"] = "右上数字"




