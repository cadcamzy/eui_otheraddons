﻿-- -------------------------------------------------------------------------- --
-- GridStatusRaidIcons zhCN Localization                                      --
-- Please make sure to save this file as UTF-8. ¶                             --
-- -------------------------------------------------------------------------- --
if GetLocale() ~= "zhCN" then return end
GridStatusRaidIcons_Locales:CreateLocaleTable(

{
	["Opacity"] = "不透明度",
	["Sets the opacity for the raid icons."] = "设置为团队图标的不透明.",
}

)