-- -------------------------------------------------------------------------- --
-- GridIndicatorCornerIcons zhCN Localization                                 --
-- Please make sure to save this file as UTF-8. ¶                             --
-- -------------------------------------------------------------------------- --
if GetLocale() ~= "zhCN" then return end
GridIndicatorCornerIcons_Locales:CreateLocaleTable(

{
	["Icon (Corners)"] = "图标（角落）",
	["Options for Icon (Corners) indicators."] = "图标（角落）指示选项。",

	["Top Left Corner Icon (Left)"] = "左上角（左）",
	["Top Left Corner Icon (Right)"] = "左上角（右）",
	["Icon Size Top Left Corner"] = "左上角图标大小",
	["Adjust the size of the 2 Top Left Corner Icons."] = "调整左上角2个图标大小。",

	["Top Right Corner Icon (Left)"] = "右上角（左）",
	["Top Right Corner Icon (Right)"] = "右上角（右）",
	["Icon Size Top Right Corner"] = "右上角图标大小",
	["Adjust the size of the 2 Top Right Corner Icons."] = "调整右上角2个图标大小。",

	["Bottom Left Corner Icon (Left)"] = "左下角（左）",
	["Bottom Left Corner Icon (Right)"] = "左下角（右）",
	["Icon Size Bottom Left Corner"] = "左下角图标大小",
	["Adjust the size of the 2 Bottom Left Corner Icons."] = "调整左下角2个图标大小。",

	["Bottom Right Corner Icon (Left)"] = "右下角（左）",
	["Bottom Right Corner Icon (Right)"] = "右下角（右）",
	["Icon Size Bottom Right Corner"] = "右下角图标大小",
	["Adjust the size of the 2 Bottom Right Corner Icons."] = "调整右下角2个图标大小。",

	["Offset X-axis"] = "X-轴 偏移量",
	["Adjust the offset of the X-axis."] = "调整X-轴的偏移量。",

	["Offset Y-axis"] = "Y-轴 偏移量",
	["Adjust the offset of the Y-axis."] = "调整Y-轴的偏移量。",

	["Icon Border Size"] = "边框宽度",
	["Adjust the size of the icon's border."] = "设置边框粗细",
	["Enable Icon Stack Text"] = "开启图标堆叠数字",
	["Toggle icon's stack count text."] = "开启图标堆叠数字",
	["Enable Icon Cooldown Frame"] = "开启图标冷却计时",
	["Toggle icon's cooldown frame."] = "开启图标冷却计时",

	["Icon Stack Text: Font Size"] = "堆叠字体大小",
	["Adjust the font size for Icon Stack Text."] = "堆叠字体大小",
	["Icon Stack Text: Offset X-axis"] = "堆叠字体横向偏移",
	["Adjust the offset for Icon Stack Text of the X-axis."] = "堆叠字体X轴偏移量",
	["Icon Stack Text: Offset Y-axis"] = "堆叠字体纵向偏移",
	["Adjust the offset for Icon Stack Text of the Y-axis."] = "堆叠字体Y轴偏移量",

	["Same settings as Grid"] = "设置为Grid默认形式",
	["If enabled, the settings for the Icon (Corners) indicators are adjustable with the standard Grid options. If deactivated, you can set individual settings for the Icon (Corners) indicators."] = "",

	["Configuration Mode"] = "测试模式",
	["Shows all Icon (Corners) indicators."] = "显示所有图标（貌似失效了）",
}

)