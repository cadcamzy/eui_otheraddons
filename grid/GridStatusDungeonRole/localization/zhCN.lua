﻿local L = LibStub("AceLocale-3.0"):NewLocale("GridStatusDungeonRole", "zhCN")
if not L then return end

if L then
    L["Dungeon Role"] = "!副本角色"
    L["Healer"] = "治疗"
    L["DPS"] = "DPS"
    L["Tank"] = "坦克"
    L["Healer color"] = "治疗颜色"
    L["Color for Healers."] = "治疗颜色"
    L["DPS color"] = "DPS颜色"
    L["Color for DPS."] = "DPS颜色"
    L["Tank color"] = "坦克颜色"
    L["Color for Tanks."] = "坦克颜色"
    L["Role filter"] = "角色过滤"
    L["Show status for the selected roles."] = "只显示勾选的角色"
    L["Show on Healer."] = "显示治疗"
    L["Show on DPS."] = "显示DPS"
    L["Show on Tank."] = "显示坦克"
    L["Hide in combat"] = "战斗中隐藏"
    L["Hide roles while in combat."] = "战斗中隐藏"
end
