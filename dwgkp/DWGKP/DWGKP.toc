## Interface: 50200
## Title: EUI[Duowan] DWGKP
## Notes: Duowan GKP
## Title-zhCN: EUI[多玩] 多玩金团助手
## Notes-zhCN: EUI多玩金团队活动记分系统
## Title-zhTW: EUI[多玩] 多玩金團助手
## Notes-zhTW: EUI多玩金團隊活動記分系統
## Author: dugu
## Version: 20130428
## SavedVariablesPerCharacter: DWGKP_DB, DWGKP_Config

Libs\load_libs.xml

#@localization
Localization\locale.xml

#@Template
Template.xml

#@dataCore
core.lua

#@view
DWGKP.xml

#@options
options.lua
LDBplugin.lua