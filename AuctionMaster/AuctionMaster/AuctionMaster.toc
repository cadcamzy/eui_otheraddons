## Interface: 50200
## Title: AuctionMaster
## Notes: Auction house selling and statistic tool.
## Author: Udorn
## RequiredDeps:
## SavedVariables: VendorDb, AuctionMasterMiscDb
## DefaultState: enabled
## Dependancies:
## Version: 5.5.12
## X-Category: Auction
## X-Feedback: http://www.curse.com/addons/wow/vendor

libs\Import.xml
src\xml\Import.xml
src\locale\Import.xml
src\main\Import.xml
src\api\Import.xml
src\test\Import.xml