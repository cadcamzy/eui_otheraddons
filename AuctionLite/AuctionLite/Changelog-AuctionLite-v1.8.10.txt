------------------------------------------------------------------------
r251 | merialkilrogg | 2013-04-01 04:24:24 +0000 (Mon, 01 Apr 2013) | 6 lines
Changed paths:
   A /tags/v1.8.10 (from /trunk:250)

Tagging as v1.8.10

Changes:
- Updated for WoW 5.2
- Fix battle pet selling bug by allowing only one to be sold at a time
- Update disenchant tables
------------------------------------------------------------------------
r250 | merialkilrogg | 2013-04-01 04:21:59 +0000 (Mon, 01 Apr 2013) | 2 lines
Changed paths:
   M /trunk/AuctionLite.lua
   M /trunk/AuctionLite.toc

Update to version 1.8.10.

------------------------------------------------------------------------
r249 | merialkilrogg | 2013-04-01 04:19:18 +0000 (Mon, 01 Apr 2013) | 4 lines
Changed paths:
   M /trunk/AuctionLite.toc
   M /trunk/CreateAuctions.lua
   M /trunk/Disenchant.lua
   M /trunk/Tooltip.lua
   M /trunk/Util.lua

Updated disenchat tables.
Fixed bug when selling multiple battle pets.
Updated to TOC 50200.

------------------------------------------------------------------------
r248 | mmosimca | 2013-01-11 14:34:54 +0000 (Fri, 11 Jan 2013) | 1 line
Changed paths:
   M /trunk/Tooltip.lua

Fixed error with BattlePetTooltip's name field (it was trying to pass BattlePetID as a name)
------------------------------------------------------------------------
