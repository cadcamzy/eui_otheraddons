﻿## Interface: 50200
## X-Min-Interface: 50001
## Title:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r |cff69ccf0Scenario-MoP|r
## Title-zhCN:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r |cff69ccf0场景战役|r
## Author: MysticalOS/Arta
## LoadOnDemand: 1
## RequiredDeps: DBM-Core
## SavedVariablesPerCharacter: DBMScenarioMoP_SavedVars, DBMScenarioMoP_SavedStats
## X-DBM-Mod: 1
## X-DBM-Mod-Type: SCENARIO
## X-DBM-Mod-Category: MOP
## X-DBM-Mod-Name: Scenario (MoP)
## X-DBM-Mod-Name-zhTW: 事件 (潘達利亞之謎)
## X-DBM-Mod-Name-zhCN: 场景战役 (熊猫人之谜)
## X-DBM-Mod-Sort: 35
## X-DBM-Mod-LoadZoneID: 906, 851, 899, 911, 920, 934, 919, 900, 878, 912, 883, 882, 884, 914, 880
localization.en.lua
localization.tw.lua
localization.cn.lua
ABrewingStorm\BrewingStorm.lua
ALittlePatience\ALittlePatience.lua
ArenaOfAnnihilation\ArenaOfAnnihilation.lua
AssaultofZanvess\AssaultofZanvess.lua
BrewmoonFestival\BrewmoonFestival.lua
CryptofForgotenKings\CryptofKings.lua
DaggerInDark\DaggerInDark.lua
GreenstoneVillage\GreenstoneVillage.lua
Landfall\Landfall.lua
TheramoresFall\Theramore.lua
TrovesThunderKing\Troves.lua
UngaIngoo\UngaIngoo.lua
WarlockSolo\GreenFire.lua
