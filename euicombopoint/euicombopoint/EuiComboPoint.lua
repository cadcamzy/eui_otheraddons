--Author: Eui.cc at 2013/03/14
--Version: 2.4
--Don't delete that above!!!

local E, _, V, P, G = unpack(ElvUI)
P.euiscript.combopoint = 1

local myclass = select(2, UnitClass('player'))
local TIMER_NUMBERS_SETS = {};
TIMER_NUMBERS_SETS["BigGold"]  = {	texture = "Interface\\Timer\\BigTimerNumbers", 
									w=256, h=170, texW=1024, texH=512,
									numberHalfWidths = {
										--0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
										35/128, 14/128, 33/128, 32/128, 36/128, 32/128, 33/128, 29/128, 31/128, 31/128,
									}
								}

local EuiComboPoint;
local L = {}
if GetLocale() == 'zhCN' then
	L["EuiComboPoint"] = "连击点"
	L["EuiComboPointScale"] = "连击点比例"
elseif GetLocale() == 'zhTW' then
	L["EuiComboPoint"] = "連擊點"
	L["EuiComboPointScale"] = "連擊點比例"
else
	L["EuiComboPoint"] = "EuiComboPoint"
	L["EuiComboPointScale"] = "EuiComboPoint Scale"
end

local a = CreateFrame("Frame", nil, UIParent)
a:RegisterEvent("PLAYER_ENTERING_WORLD")
a:RegisterEvent("ADDON_LOADED")

local function SetComboPointScale()
	local combopoint = E.db.euiscript.combopoint

	EuiComboPoint.digit1:SetSize(EuiComboPoint.style.w * combopoint, EuiComboPoint.style.h * combopoint);
	EuiComboPoint.digit2:SetSize(EuiComboPoint.style.w * combopoint, EuiComboPoint.style.h * combopoint);
	--This is to compensate texture size not affecting GetWidth() right away.
	EuiComboPoint.digit1.width, EuiComboPoint.digit2.width = EuiComboPoint.style.w * combopoint, EuiComboPoint.style.w * combopoint;
end

a:SetScript("OnEvent", function(self, event, addon)
	if event == "PLAYER_ENTERING_WORLD" then
		self:UnregisterEvent("PLAYER_ENTERING_WORLD")
		local CAN_HAVE_CLASSBAR = (myclass == "PALADIN" or myclass == 'ROGUE' or myclass == "DRUID" or myclass == "WARLOCK" or myclass == "PRIEST" or myclass == "MONK" or myclass == 'MAGE')

		if CAN_HAVE_CLASSBAR then
			if not EuiComboPoint then EuiComboPoint = CreateFrame("FRAME", nil, UIParent, "EuiStartTimerBar"); end
		end
		if not EuiComboPoint then return end
		EuiComboPoint.style = TIMER_NUMBERS_SETS["BigGold"];
			
		EuiComboPoint.digit1:SetTexture(EuiComboPoint.style.texture);
		EuiComboPoint.digit2:SetTexture(EuiComboPoint.style.texture);
				
		EuiComboPoint.digit1.glow = EuiComboPoint.glow1;
		EuiComboPoint.digit2.glow = EuiComboPoint.glow2;
		EuiComboPoint.glow1:SetTexture(EuiComboPoint.style.texture.."Glow");
		EuiComboPoint.glow2:SetTexture(EuiComboPoint.style.texture.."Glow");	
		EuiComboPoint:ClearAllPoints();
		EuiComboPoint:SetPoint("TOP", 0, -255);	
		EuiComboPoint.digit1:SetAlpha(0)
		EuiComboPoint.digit2:SetAlpha(0)
		
		local anchor = CreateFrame("Frame", "EuiComboPointAnchor", UIParent)
		anchor:SetSize(50, 50)
		anchor:SetPoint("CENTER")
		E:CreateMover(anchor, "EuiComboPointAnchorMover", L["EuiComboPoint"])
		
		EuiTimerTracker_OnLoad(EuiComboPoint)		
		SetComboPointScale()		
	elseif event == "ADDON_LOADED" and addon == 'ElvUI_Config' then	
		E.Options.args.combopoint = {
			type = 'group',
			name = L["EuiComboPoint"],
			order = -20, --Always Last Hehehe
			args = {
				combopoint = {
					order = 200,
					type = "range",
					name = L["EuiComboPointScale"],
					get = function(info) return E.db.euiscript.combopoint end,
					set = function(info, value) E.db.euiscript.combopoint = value; SetComboPointScale(); end,
					min = 0.1, max = 2, step = 0.1,
				},
			},
		}
	end
end)
								
function EuiTimerTracker_OnLoad(self)
	self:RegisterEvent("UNIT_COMBO_POINTS");
	self:RegisterEvent("PLAYER_TARGET_CHANGED");
	self:RegisterEvent("UNIT_POWER")
	self:RegisterEvent("UNIT_DISPLAYPOWER")
	self:RegisterEvent("PLAYER_LEVEL_UP")
	if myclass == 'MAGE' then
		self:RegisterEvent("UNIT_AURA")
	end
	self:SetScript("OnEvent", EuiTimerTracker_OnEvent)
end


function EuiTimerTracker_OnEvent(self, event, ...)
	local cp = 0;

	if event == 'UNIT_AURA' and myclass == 'MAGE' then
		if GetSpecialization() == 1 then
			for i = 1, 40 do
				local count, _, start, timeLeft, _, _, _, spellID = select(4, UnitDebuff('player', i))
				if spellID == 36032 then
					cp = count or 0
				end
			end
		end
	else
		if myclass == "DRUID" or myclass == "ROGUE" then
			if(UnitHasVehicleUI'player') then
				cp = GetComboPoints('vehicle', 'target')
			else
				cp = GetComboPoints('player', 'target')
			end
		elseif myclass == "PALADIN" then
			cp = UnitPower('player', SPELL_POWER_HOLY_POWER)
		elseif myclass == 'PRIEST' then
			cp = UnitPower("player", SPELL_POWER_SHADOW_ORBS)
		elseif myclass == 'WARLOCK' then
			local spec = GetSpecialization()
			if (spec == SPEC_WARLOCK_DESTRUCTION) then	
				cp = floor(UnitPower("player", SPELL_POWER_BURNING_EMBERS, true) / 10)
			elseif (spec == SPEC_WARLOCK_AFFLICTION) then
				cp = UnitPower("player", SPELL_POWER_SOUL_SHARDS)
			end
		elseif myclass == 'MONK' then
			cp = UnitPower("player", SPELL_POWER_CHI)
		end
	end
		
	if cp > 0 then 
		EuiComboPoint.digit1:SetAlpha(1)
		EuiComboPoint.digit2:SetAlpha(1)
	end
	if EuiComboPoint.cp == cp then return; end
	
	EuiComboPoint.startNumbers:Stop()
	EuiComboPoint.cp = cp;
	EuiComboPoint.startNumbers:Play()
end

function EuiStartTimer_SetTexNumbers(self, ...)
	local digits = {...}
	local cpDigits = floor(self.cp);
	local digit;
	local style = self.style;
	local i = 1;
	
	local texCoW = style.w/style.texW;
	local texCoH = style.h/style.texH;
	local l,r,t,b;
	local columns = floor(style.texW/style.w);
	local numberOffset = 0;
	
	while digits[i] do -- THIS WILL DISPLAY SECOND AS A NUMBER 2:34 would be 154
		if cpDigits > 0 then
			digit = mod(cpDigits, 10);
			
			digits[i].hw = style.numberHalfWidths[digit+1]*digits[i].width;
			numberOffset  = numberOffset + digits[i].hw;
			
			l = mod(digit, columns) * texCoW;
			r = l + texCoW;
			t = floor(digit/columns) * texCoH;
			b = t + texCoH;
			digits[i]:SetTexCoord(l,r,t,b);
			digits[i].glow:SetTexCoord(l,r,t,b);
			
			cpDigits = floor(cpDigits/10);	
			digits[i]:ClearAllPoints();
			digits[i]:SetPoint("CENTER", EuiComboPointAnchor, "CENTER", numberOffset - digits[i].hw, 0);
		else
			digits[i]:SetTexCoord(0,0,0,0);
			digits[i].glow:SetTexCoord(0,0,0,0);
		end
		
		i = i + 1;
	end
end