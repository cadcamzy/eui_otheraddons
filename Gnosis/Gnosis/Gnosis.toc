## Interface: 50200
## Title: Gnosis (|cffaaff44Castbars and Timers|r) r58
## Notes: Highly configurable castbar and single timer addon.
## Author: elchefe <>
## Version: r58
## SavedVariables: GnosisDB, GnosisConfigs
## SavedVariablesPerCharacter: GnosisChar, GnosisCharConfig
## OptionalDeps: Ace3, LibSharedMedia-3.0, AceGUI-3.0-SharedMediaWidgets
## X-Embeds: Ace3, LibSharedMedia-3.0, AceGUI-3.0-SharedMediaWidgets
## LoadOnDemand: 0
## DefaultState: enabled
## X-Curse-Packaged-Version: r58
## X-Curse-Project-Name: Gnosis (Castbars and Timers)
## X-Curse-Project-ID: gnosis
## X-Curse-Repository-ID: wow/gnosis/mainline

# Libraries
#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml
Libs\AceDB-3.0\AceDB-3.0.xml
Libs\AceEvent-3.0\AceEvent-3.0.xml
Libs\LibSharedMedia-3.0\lib.xml
Libs\AceGUI-3.0-SharedMediaWidgets\widget.xml
Libs\LibRangeCheck-2.0\LibRangeCheck-2.0.lua
Libs\LibDialog-1.0\lib.xml
Libs\LibBetterBlizzOptions-1.0\LibBetterBlizzOptions-1.0\lib.xml
#@end-no-lib-strip@

# xml
Gnosis.xml

# lua
Gnosis.lua
Locale.lua
Callback.lua
OptionsFuncs.lua
Options.lua
Variables.lua
Bars.lua
Timers.lua

# Gnosis' StatusBar implementation
CreateStatusBar.lua
