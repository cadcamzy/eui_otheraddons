## Interface: 50200
## Title: CoolLine
## Notes: Shows cooldowns on a single bar.
## Author: TotalPackage
## Version: 5.2.002

## SavedVariables: CoolLineDB
## SavedVariablesPerCharacter: CoolLineCharDB

## OptionalDeps: LibSharedMedia-3.0

## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed

embeds.xml
core.lua
