﻿## Interface: 50100
## Title:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r Throne of the Four Winds
## Title-zhCN:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r 风神王座
## Title-zhTW:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r 四風王座
## Notes-zhCN: |cff00ff00DBM-ThroneFourWinds|r
## Notes-zhTW: |cff00ff00DBM-ThroneFourWinds|r
## Author: Nitram/Tandanu
## RequiredDeps: DBM-Core
## SavedVariablesPerCharacter: DBMThroneFourWinds_SavedVars, DBMThroneFourWinds_SavedStats
## X-DBM-Mod: 1
## X-DBM-Mod-Category: CATA
## X-DBM-Mod-Name: Throne of the Four Winds
## X-DBM-Mod-Name-zhCN: 风神王座
## X-DBM-Mod-Name-zhTW: 四風王座
## X-DBM-Mod-Sort: 105
## X-DBM-Mod-LoadZoneID: 773
## X-Revision: BigFoot
## LoadOnDemand: 1

localization.en.lua
localization.tw.lua
localization.cn.lua

MapSizes.lua
Conclave.lua
AlAkir.lua
